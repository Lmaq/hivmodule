<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OPDpatient;

class OPDController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patients=OPDpatient::all();
        //dd('patients');
        return view('OPD.index',compact('patients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('OPD.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'serial_number'=>'required',
            'muac_cm'=>'required',
            'mauc'=>'required',
            'weight'=>'required',
            'height'=>'required',
            'bmi'=>'required',
            'age_of_weight_zscore'=>'required',
            'height_of_age_zscore'=>'required',
            'blood_pressure_systolic'=>'required',
            'blood_pressure_systolic'=>'required',
            'blood_sugar'=>'required',
            'temp'=>'required',
            'next_of_kin'=>'required',
            'palliative_care'=>'required',
            'alcohol'=>'required',
            'tobacco'=>'required',
            'results_of_new_presumed_case'=>'required',
        ]);
        $saveOPDpatient=new OPDpatient;
        $saveOPDpatient->serial_number =$request->serial_number;
        $saveOPDpatient->visit_date=$request->visit_date;
        $saveOPDpatient->muac_cm =$request->muac_cm;
        $saveOPDpatient->mauc =$request->mauc;
        $saveOPDpatient->weight =$request->weight;
        $saveOPDpatient->height =$request->height;
        $saveOPDpatient->bmi =$request->bmi;
        $saveOPDpatient->age_of_weight_zscore =$request->age_of_weight_zscore;
        $saveOPDpatient->height_of_age_zscore =$request->height_of_age_zscore;
        $saveOPDpatient->blood_pressure_diastolic =$request->blood_pressure_diastolic;
        $saveOPDpatient->blood_pressure_systolic =$request->blood_pressure_systolic;
        $saveOPDpatient->blood_sugar =$request->blood_sugar;
        $saveOPDpatient->temp =$request->temp;
        $saveOPDpatient->next_of_kin =$request->next_of_kin;
        $saveOPDpatient->palliative_care =$request->palliative_care;
        $saveOPDpatient->patient_classification =$request->patient_classification;
        $saveOPDpatient->tobacco =$request->tobacco;
        $saveOPDpatient->alcohol =$request->alcohol;
        $saveOPDpatient->fever =$request->fever;
        $saveOPDpatient->test_done =$request->test_done;
        $saveOPDpatient->results =$request->results;
        $saveOPDpatient->results_of_new_presumed_case =$request->results_of_new_presumed_case;
        $saveOPDpatient->sent_to_lab =$request->sent_to_lab;
        $saveOPDpatient->lab_test_results =$request->lab_test_results;
        $saveOPDpatient->linked_to_TB =$request->linked_to_TB;
        $saveOPDpatient->drug = implode(',', $request->drug);
        //$saveOPDpatient->drugs= explode(',', $request->drug);
        $saveOPDpatient->diagnosis= implode(',', $request->diagnosis);
        //$saveOPDpatient->diagnoses= explode(',', $request->diagnosis);
        $saveOPDpatient->units_per_day = implode(',', $request->units_per_day);
        //$saveOPDpatient->units_per_days= explode(',', $request->units_per_day);
        $saveOPDpatient->doses_per_day =$request->doses_per_day;
        $saveOPDpatient->time1 = implode(',', $request->time1);
        //$saveOPDpatient->time1s= explode(',', $request->time1);
        $saveOPDpatient->time2 =$request->time2;
        $saveOPDpatient->disability =$request->disability;
        $saveOPDpatient->type_of_disability =$request->type_of_disability;
        $saveOPDpatient->device_provided =$request->device_provided;
        $saveOPDpatient->refferal_in_number =$request->refferal_in_number;
        $saveOPDpatient->refferal_out_number =$request->refferal_out_number;
        $saveOPDpatient->save();

        // if (count($request->diagnosis)>0) {
        //     foreach ($request->diagnosis as $diagnose) {
        //         $data = array(
        //             'diagnosis' =>$request->diagnosis[$diagnose] ,
        //             'drug' =>$request->drug[$diagnose],
        //             'units_per_day' =>$request->units_per_day[$diagnose],
        //             'time1'=>$request->time1[$diagnose]               
        //         );
        //     }
        // }
        return redirect()->route('OPD.index')->with('success','New patient saved successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $patients=OPDpatient::find($id);
        return view('OPD.show',compact('patients'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $patients=OPDpatient::findOrFail($id);
        $diagnose=explode(',', $patients->diagnosis);
        $drugs=explode(',', $patients->drug);
        $units_per_days=explode(',', $patients->units_per_day);
        $time1s=explode(',', $patients->time1);


        return view('OPD.edit',compact('patients','diagnose','drugs','units_per_days','time1s'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'serial_number'=>'required',
            'muac_cm'=>'required',
            'mauc'=>'required',
            'weight'=>'required',
            'height'=>'required',
            'bmi'=>'required',
            'age_of_weight_zscore'=>'required',
            'height_of_age_zscore'=>'required',
            'blood_pressure_systolic'=>'required',
            'blood_pressure_systolic'=>'required',
            'blood_sugar'=>'required',
            'temp'=>'required',
            'next_of_kin'=>'required',
            'palliative_care'=>'required',
            'alcohol'=>'required',
            'tobacco'=>'required',
            'results_of_new_presumed_case'=>'required',
        ]);
        $saveOPDpatient=new OPDpatient;
        $saveOPDpatient->serial_number =$request->serial_number;
        $saveOPDpatient->visit_date=$request->visit_date;
        $saveOPDpatient->muac_cm =$request->muac_cm;
        $saveOPDpatient->mauc =$request->mauc;
        $saveOPDpatient->weight =$request->weight;
        $saveOPDpatient->height =$request->height;
        $saveOPDpatient->bmi =$request->bmi;
        $saveOPDpatient->age_of_weight_zscore =$request->age_of_weight_zscore;
        $saveOPDpatient->height_of_age_zscore =$request->height_of_age_zscore;
        $saveOPDpatient->blood_pressure_diastolic =$request->blood_pressure_diastolic;
        $saveOPDpatient->blood_pressure_systolic =$request->blood_pressure_systolic;
        $saveOPDpatient->blood_sugar =$request->blood_sugar;
        $saveOPDpatient->temp =$request->temp;
        $saveOPDpatient->next_of_kin =$request->next_of_kin;
        $saveOPDpatient->palliative_care =$request->palliative_care;
        $saveOPDpatient->patient_classification =$request->patient_classification;
        $saveOPDpatient->tobacco =$request->tobacco;
        $saveOPDpatient->alcohol =$request->alcohol;
        $saveOPDpatient->fever =$request->fever;
        $saveOPDpatient->test_done =$request->test_done;
        $saveOPDpatient->results =$request->results;
        $saveOPDpatient->results_of_new_presumed_case =$request->results_of_new_presumed_case;
        $saveOPDpatient->sent_to_lab =$request->sent_to_lab;
        $saveOPDpatient->lab_test_results =$request->lab_test_results;
        $saveOPDpatient->linked_to_TB =$request->linked_to_TB;
        $saveOPDpatient->drug = implode(',', $request->drug);
        $saveOPDpatient->diagnosis= implode(',', $request->diagnosis);
        $saveOPDpatient->units_per_day = implode(',', $request->units_per_day);
        $saveOPDpatient->doses_per_day =$request->doses_per_day;
        $saveOPDpatient->time1 = implode(',', $request->time1);
        $saveOPDpatient->time2 =$request->time2;
        $saveOPDpatient->disability =$request->disability;
        $saveOPDpatient->type_of_disability =$request->type_of_disability;
        $saveOPDpatient->device_provided =$request->device_provided;
        $saveOPDpatient->refferal_in_number =$request->refferal_in_number;
        $saveOPDpatient->refferal_out_number =$request->refferal_out_number;
        //OPDpatient::find($id)->update($request->all());
        //OPDpatient::find($id)->update($request->all());
        return redirect()->route('OPD.index')->with('success','patient updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        OPDpatient::find($id)->delete();
        return redirect()->route('OPD.index')->with('success','patient deleted successfully');
    }
}
