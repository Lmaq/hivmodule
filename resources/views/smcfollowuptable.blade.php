@extends('layouts.app')

@section('table')
<div class="container">
 @include('message')
    <a href="/malecircumsion/create" class="btn btn-info">Add Follow up</a>
        <h2 class="pull-center">Circumcised Patient Follow up List</h2>
        <table class="table table-bordered table-striped" id="users-table">  
    <thead>
    <tr class="headers" style="text-align: center;">

    <th colspan="4" scope="colgroup">Facility Information</th> 
    <th colspan="3" scope="colgroup">Circumcision Follow Up Visits</th>
    <th colspan="2" scope="colgroup">Action</th>
    </tr>
    <tr>
                <th scope="col">Site Type</th>
                <th scope="col">Clinic Team Leader</th> 
                <th scope="col">National ID</th>
                <th scope="col">Serial ID</th> 
                <th scope="col">Date</th>
                <th scope="col">Wound status</th>
                <th scope="col">Misssed Apt</th>
                <th scope="col">More</th>
                <th scope="col">Edit/Delete</th>
                
        
    </tr>
</thead>
</table>
</div>
@endsection
@push('scripts')
<script>
$(document).ready(function(){
    $('#users-table').DataTable({
        processing:true,
        serverSide:true,
        ajax:{
            url:"{{route('malecircumsion.index')}}",
        },
        columns:[
            {
                data:'sitetype',
                name:'sitetype',

            },
            {
                data:'CTL',
                name:'CTL',
            },
            {
                data:'NatID',
                name:'NatID',
            },
            {
                data:'SerID',
                name:'SerID',
            },
            {
                data:'Date',
                name:'Date',
            },
            {
                data:'wound',
                name:'wound',
            },
            {
                data:'missedapt',
                name:'missedapt',
            },
            {
                data:'more',
                name:'more',
               
            },
            {
                data:'action',
                name:'action',
                orderable:false
            }
        ]
    });
    $('#users-table').on('click', '.btn-delete[data-remote]', function (e) { 
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var url = $(this).data('remote');
    // confirm then
    if (confirm('Are you sure you want to delete this?')) {
    $.ajax({
        url: url,
        type: 'DELETE',
        dataType: 'json',
        data: {method: '_DELETE', submit: true}
    }).always(function (data) {
        $('#users-table').DataTable().draw(false);
    }); }else
        alert("You have cancelled!");
});
});
</script>
@endpush