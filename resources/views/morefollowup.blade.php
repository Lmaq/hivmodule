@extends('layouts.app')

@section('table')
<div class="container">
    <a href="/malecircumsion" class="btn btn-info">Back</a>
        <h2 class="pull-center">More About Patient </h2>
        <table class="table table-bordered table-striped" id="users-table">
    <thead>
    <tr class="headers" style="text-align: center;">
    <th colspan="4" scope="colgroup">Circumcision Follow Up Visits</th>
    </tr>
    <tr>
               
                <th scope="col">Type of Visit</th>
                <th scope="col">Presence of AE</th>
                <th scope="col">Severity</th> 
                <th scope="col">Treatment given</th>
                
        
    </tr>
</thead>
</table>
</div>
@endsection
@push('scripts')
<script>
$(document).ready(function(){
    $('#users-table').DataTable({
        processing:true,
        serverSide:true,
        ajax:{
            url:"{{route('malecircumsion.index')}}",
        },
        columns:[
            {
                data:'visittype',
                name:'visittype',

            },
            {
                data:'AE',
                name:'AE',
            },
            {
                data:'severity',
                name:'severity',
            },
            {
                data:'tmtgiven',
                name:'tmtgiven',
                orderable:false
            }
               
        ]
    });
});
</script>
@endpush