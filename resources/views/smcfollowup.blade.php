@extends('layouts.app')

@section('follow')

<div class="container">
@include('errrors')
        <form action="/malecircumsion/@yield('id')" method="post">
    {{csrf_field()}}
    @section('editMethod') 
        @show
        <div>
            <div class="headers" style="text-align: center;">
                <h3>Safe Male Circumcision Client Card Follow Up</h3>
            </div>
            <section>
                <h3>Facility Information</h3>
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                    
                        <td   form-inline>
                            <label>Site Type:</label>
                                  
                                    {!! Form::select('types', array('Choose...','Static/Fixed' => 'Static/Fixed', 'Outreach' => 'Outreach', 'Camp' => 'Camp'),[0],['class'=>'form-control']); !!}  
                         </td>
                            <td>
                                &nbsp
                            <label><span class="required">*</span>Clinic Team Leader:</label>
                          
                        
                                    {!! Form::select('cprovider', array('Choose...','Obua Samuel' => 'Obua Samuel', 'Diana Kalema' => 'Diana Kalema', 'Ivan Mukasa' => 'Ivan Mukasa', 'Denis Okello' => 'Denis Okello', 'Debora Joyner' => 'Debora Joyner', 'Dianah Nankunda' => 'Dianah Nankunda', 'Dan Kato' => 'Dan Kato', 'Opio Okello' => 'Opio Okello'),[0],['class'=>'form-control']); !!}
                                 
                        </td>
                      
                        <td form-inline>
                            <label>National ID:</label>
                            <input type="text"  name="natid" value="@yield('A')">
                                        &nbsp
                        </td>
                        <td>
                            <label>Serial ID:</label>
                            <input type="text" name="serid" value="@yield('ser')">
                        </td>
                    </tr>
                    </tbody>
                </table>
            </section>

            <section>
                <h3>Circumcision Follow Up Visits</h3>
                <table class="table table-bordered">
                <tbody groupingConceptId="99700">
                        <tr>
                            <td >
                                <label>Date:</label>
                                <input type="date" default="today" name="today" value="@yield('date')">
                            </td>
                            <td>
                                <label>Wound status</label>
                                <input type="text" name="wstatus" value="@yield('wound')">
                            </td>
                            <td>
                            <?php
                                    $AE_array = explode(',', $smcfollowup->missedapt);
                                ?>

                            @for($i=0; $i < count($AE_array) ; $i++)
                                {!! Form::checkbox('missedapt', 'Misssed Apt',  ($AE_array[$i] == "Misssed Apt") ? true : false)  !!} 
                                <label>Misssed Apt?</label>
                            @endfor
                            </td>
                        </tr>

                        <tr>
                            <td colspan="3">
                                        {!! Form::label('Type of Visit:') !!}
                                        {!! Form::radio('visit', 'Routine', isset($smcfollowup) ? $smcfollowup->visittype === 'Routine' : false) !!}
                                        <label>Routine</label>
                                        {!! Form::radio('visit', 'Client initiated/unscheduled', isset($smcfollowup) ? $smcfollowup->visittype === 'Clientinitiated/unscheduled' : false) !!}
                                        <label>Client initiated/unscheduled</label>
                                        {!! Form::radio('visit', 'recommended_by_physician', isset($smcfollowup) ? $smcfollowup->visittype === 'recommended_by_physician' : false) !!}
                                        <label>recommended by physician</label>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2" class="SMCOtherEnableDisable4">
                                <?php
                                    $AE_array = explode(',', $smcfollowup->AE);
                                ?>

                                @for($i=0; $i < count($AE_array) ; $i++)
                                    {!! Form::label('Presence of AE:') !!}
                                    {!! Form::checkbox('AE[]', 'Damage to penis/Urethra', $AE_array[$i] == "Damage to penis/Urethra" ? true : false); !!} 
                                    <label>Damage to penis/Urethra:</label>
                                    {!! Form::checkbox('AE[]', 'Excessive bleeding', ($AE_array[$i] == "Excessive bleeding") ? true : false); !!}
                                    <label>Excessive bleeding</label>
                                    {!! Form::checkbox('AE[]', 'Pus discharge', ($AE_array[$i] == "Pus discharge") ? true : false); !!}
                                    <label>Pus discharge</label>
                                    {!! Form::checkbox('AE[]', 'Excessive Swelling', ($AE_array[$i] == "Excessive Swelling") ? true : false); !!}
                                    <label>Excessive Swelling</label>
                                    {!! Form::checkbox('AE[]', 'Infection Smell', ($AE_array[$i] == "Infection Smell") ? true : false); !!}
                                    <label>Infection,Smell</label>
                                    {!! Form::checkbox('AE[]', 'Device displacement', ($AE_array[$i] == "Device displacement") ? true : false); !!}
                                    <label>Device displacement</label>
                                    {!! Form::checkbox('AE[]', 'Failure to pass urine', ($AE_array[$i] == "Failure to pass urine") ? true : false); !!}
                                    <label>Failure to pass urine</label>
                                    {!! Form::checkbox('AE[]', 'Other Specify', ($AE_array[$i] == "Other Specify") ? true : false); !!}
                                        <label>Other Specify</label>
                                @endfor
                            </td>
                            <td class="SMCOtherEnableDisableChild4">
                                <span >
                                <label>Specify</label><br>
                                <input type="text" name="AE[]" />
                                </span>

                            </td>
                        </tr>

                        <tr>
                            <td colspan="3">
                                {{ Form::label('Severity:') }}
                                {!! Form::radio('severity', 'Moderate', isset($smcfollowup) ? $smcfollowup->severity === 'Moderate' : false); !!}
                                {{ Form::label('Moderate') }}
                                {!! Form::radio('severity', 'Severe', isset($smcfollowup) ? $smcfollowup->severity === 'Severe' : false); !!} 
                                {{ Form::label('Severe') }}
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <label>Treatment given:</label>
                                <input type="text" name="ttgiven" value="@yield('ttgiven')"/>
                            </td>
                        </tr>

                        </tbody>
                </table>
            </section>
                        <section>
                                <div class="form-group">
                                                
                                        <align-left><button type="submit" class="btn btn-primary">
                                            EnterForm
                                        </button></align-left>
                                
                                </div>
                        </section>
        </div>
   
    </form>
</div>
@endsection