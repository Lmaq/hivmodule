@extends('layouts.main') 

@push('styles')
    <link href="{{ asset('elite/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Appointment follow up</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}">Dashboard</a></li>
                <li><a href="{{ route('hiv.menu') }}">HIV menu</a></li>
                <li class="active">Appointment follow up</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            @include('layouts.custom_header')
            @include('allergies.header')
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            @include('flash::message')
            <div class="white-box">
                {{ Form::open(['route' => 'hct.store','data-toggle'=>'validator']) }} 
                <div class="headers" style="text-align: center;">
                    <h3 style="background: #FFFF7D; padding: 10px;">HMIS 053 Appointment Book - Followup</h3>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            {{ Form::label('date','Date :') }}
                            <div class="input-group">
                                {{ Form::text('follow_up_date','',['class' => 'form-control compulsory','readonly','id'=>'follow_up_date']) }}
                                <span class="input-group-addon"><i class="icon-calender"></i></span>                   
                            </div>
                        </div>
                    
                        <div class="form-group">
                            {{ Form::label('type_of_care','Type of care:') }}<br>
                            {{ Form::checkbox('type_of_care[]', 'Pre ART', false) }} Pre ART &nbsp;
                            {{ Form::checkbox('type_of_care[]', 'ART', false) }} ART &nbsp;
                            {{ Form::checkbox('type_of_care[]', 'HIV Exposed Infant', false) }} HIV Exposed Infant &nbsp;
                        </div>
                    
                        <div class="form-group">
                            {{ Form::label('follow_up_type','Followup Type :') }}<br>
                            {{ Form::checkbox('follow_up_type[]', 'SMS Message', false) }} SMS Message &nbsp; <br>
                            {{ Form::checkbox('follow_up_type[]', 'Phone call', false) }} Phone Call &nbsp; <br>
                            {{ Form::checkbox('follow_up_type[]', 'Home visit', false) }} Home visit &nbsp; <br>
                            {{ Form::checkbox('follow_up_type[]', 'Other', false, ['id' => 'other_follow_up_chbx']) }} Other &nbsp;
                        </div>

                        <div class="form-group" id="other_follow_up_div" style="display: none;">
                            {{ Form::label('other_follow_up_type','Specify other follow up type :') }}
                            {{ Form::text('other_follow_up_type','', ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="col-sm-2"></div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            {{ Form::label('outcome','Outcome :') }} <br>
                            {{ Form::checkbox('outcome[]', 'Spoke with patient', false) }} Spoke with patient &nbsp; <br>
                            {{ Form::checkbox('outcome[]', 'Phone not answered', false) }} Phone not answered &nbsp; <br>
                            {{ Form::checkbox('outcome[]', 'Spoke with someone else', false) }} Spoke with someone else &nbsp; <br>
                            {{ Form::checkbox('outcome[]', 'Telephone off', false) }} Telephone off &nbsp; <br>
                            {{ Form::checkbox('outcome[]', 'Client Visit Rescheduled', false, ['id' => 'client_visit_rescheduled_chbx']) }} Client Visit Rescheduled &nbsp; <br>
                            {{ Form::checkbox('outcome[]', 'Other', false, ['id' => 'other_outcome_chbx']) }} Other specify &nbsp;
                        </div>

                        <div class="form-group" id="client_visit_rescheduled_div" style="display: none;">
                            {{ Form::label('date','Reschedule Date :') }}
                            <div class="input-group">
                                {{ Form::text('reschedule_date','',['class' => 'form-control compulsory','readonly','id'=>'reschedule_date']) }}
                                <span class="input-group-addon"><i class="icon-calender"></i></span>                   
                            </div>
                        </div>

                        <div class="form-group" id="other_outcome_div" style="display: none;">
                            {{ Form::label('other_outcome','Specify other outcome :') }}
                            {{ Form::text('other_outcome','', ['class' => 'form-control']) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('next_step','Next step :') }}<br>
                            {{ Form::checkbox('next_step[]', 'Call Treatment Supporter', false) }} Call Treatment Supporter &nbsp; <br>
                            {{ Form::checkbox('next_step[]', 'Try to followup patient again', false, ['id' => 'follow_up_again_on_chbx']) }} Try to followup patient again &nbsp; <br>
                            {{ Form::checkbox('next_step[]', 'Home visit', false, ['id' => 'home_visit_chbx']) }} Home visit &nbsp; <br>
                        </div>

                        <div class="form-group" id="follow_up_again_on_div" style="display: none;">
                            {{ Form::label('date','Follow up again on:') }}
                            <div class="input-group">
                                {{ Form::text('follow_up_again_date','',['class' => 'form-control compulsory','readonly','id'=>'follow_up_again_date']) }}
                                <span class="input-group-addon"><i class="icon-calender"></i></span>                   
                            </div>
                        </div>

                        <div class="form-group" id="home_visit_on_div" style="display: none;">
                            {{ Form::label('date','Home visit on:') }}
                            <div class="input-group">
                                {{ Form::text('home_visit_date','',['class' => 'form-control compulsory','readonly','id'=>'home_visit_date']) }}
                                <span class="input-group-addon"><i class="icon-calender"></i></span>                   
                            </div>
                        </div>

                        <div class="form-group">
                            <div style="float: right;">{{ Form::submit('Submit',['class'=>'btn btn-success']) }}</div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection 

@push('scripts')
   <script src="{{ asset('elite/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
   <script type="text/javascript">
       jQuery('#follow_up_date,#reschedule_date,#home_visit_date,#follow_up_again_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy'
        });
        $(document).ready(function(){
            $('#other_follow_up_chbx').click(function(){
                $('#other_follow_up_chbx').prop('checked') ? $("#other_follow_up_div").show() : $("#other_follow_up_div").hide();
            });
            $('#client_visit_rescheduled_chbx').click(function(){
                $('#client_visit_rescheduled_chbx').prop('checked') ? $("#client_visit_rescheduled_div").show() : $("#client_visit_rescheduled_div").hide();
            });
            $('#other_outcome_chbx').click(function(){
                $('#other_outcome_chbx').prop('checked') ? $("#other_outcome_div").show() : $("#other_outcome_div").hide();
            });
            $('#follow_up_again_on_chbx').click(function(){
                $('#follow_up_again_on_chbx').prop('checked') ? $("#follow_up_again_on_div").show() : $("#follow_up_again_on_div").hide();
            });
            $('#home_visit_chbx').click(function(){
                $('#home_visit_chbx').prop('checked') ? $("#home_visit_on_div").show() : $("#home_visit_on_div").hide();
            });
        });
   </script>
@endpush