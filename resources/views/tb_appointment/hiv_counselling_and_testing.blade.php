@extends('layouts.main') 

@push('styles')
    <link href="{{ asset('elite/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">HIV Counselling and testing</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}">Dashboard</a></li>
                <li><a href="{{ route('hiv.menu') }}">HIV menu</a></li>
                <li class="active">HIV Counselling</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            @include('layouts.custom_header')
            @include('allergies.header')
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            @include('flash::message')
            <div class="white-box">
                {{ Form::open(['route' => 'hct.store','data-toggle'=>'validator']) }} 
                <div class="row">
                    <div class="headers" style="text-align: center;">
                        <h3 style="background: #FFFF7D; padding: 10px;">HIV Counselling and Testing Client Card</h3>
                    </div>
                    <!-- =================== -->
                    <div class="col-sm-12">
                        <h3>Section A:</h3>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            {{ Form::label('date','Date :') }}
                            <div class="input-group">
                                {{ Form::text('counselling_date','',['class' => 'form-control compulsory','readonly','id'=>'counselling_date']) }}
                                <span class="input-group-addon"><i class="icon-calender"></i></span>                   
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            {{ Form::label('is_center_static','Is the Center static or an Outreach?:') }}
                            {{ Form::select('is_center_static',['static'=>'Static','outreach'=>'Outreach'],'',['class' => 'form-control', 'required', 'data-error'=>'','placeholder'=>'']) }}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            {{ Form::label('registration_number','Registration number :') }}
                            {{ Form::text('registration_number','',['class' => 'form-control', 'data-error'=>'','placeholder'=>'']) }}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <!-- display this only if the patient is less than 12 years -->
                        <div class="form-group">
                            {{ Form::label('accompanied_by','Accompanied by :') }}
                            {{ Form::select('accompanied_by', [''=>'--Select--','mother'=>'Mother', 'father'=>'Father', 'guardian'=>'Guardian'], '', ['class' => 'form-control', 'data-error'=>'', 'placeholder'=>'']) }}
                        </div>
                    </div>
                    <!-- =================== -->

                    <div class="col-sm-12">
                        <h3>Section B:</h3>
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td style="width: 40%">
                                    {{ Form::label('pre_test_counselling_done','Pre -Test Counseling done/information given:') }}
                                </td>
                                <td>
                                    {{ Form::select('accompanied_by', [''=>'--Select--','0'=>'No', '1'=>'Yes'], '', ['class' => 'form-control']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Counseled as:</label>
                                </td>
                                <td>
                                    {{ Form::select('counselled_as', [''=>'--Select--','1'=>'Individual', '2'=>'Couple','3'=>'Group'], '', ['class' => 'form-control']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Approach:</label>
                                </td>
                                <td>
                                    {{ Form::select('approach', [''=>'--Select--','1'=>'CICT', '2'=>'PITC'], '', ['class' => 'form-control']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>HCT Entry Point:</label>
                                </td>
                                <td>
                                    {{ Form::select('hct_entry_point', [''=>'--Select--','1'=>'Facility based', '2'=>'Work place', '3'=>'Comm/Out reach','4'=>'HBHCT','5'=>'Circumcision (SMC)','6'=>'PEP','7'=>'PMTCT','8'=>'MARPS','9'=>'ANC','10'=>'Maternity','11'=>'Family Planning','12'=>'Gynecological OPD','13'=>'Emergency Gynecological ward','14'=>'Men&#39;s Access Clinic','15'=>'Mother Baby Pair Clinic'], '', ['class' => 'form-control']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Have you ever tested for HIV before?:</label>
                                </td>
                                <td>
                                    {{ Form::select('ever_tested_for_hiv_before', [''=>'--Select--','0'=>'No', '1'=>'Yes'], '', ['class' => 'form-control']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>How many times have you tested in the last 12 months?:</label>
                                </td>
                                <td>
                                    {{ Form::text('times_tested_in_last_12_months','',['class' => 'form-control', 'data-error'=>'','placeholder'=>'']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Number of sexual partners in the last 12 months:</label>
                                </td>
                                <td>
                                    {{ Form::text('number_of_sexual_partners_in_last_12_months','',['class' => 'form-control', 'data-error'=>'','placeholder'=>'']) }}
                                </td>
                            </tr>
                            <!-- display this if the patient is abve 9 years -->
                            <tr>
                                <td>
                                    <label>Has your spouse /partner been tested for HIV before? :</label>
                                </td>
                                <td colspan="3" class="PartnerTestedBeforeEnableDisable1">
                                    {{ Form::select('partner_tested_before', [''=>'--Select--','0'=>'No', '1'=>'Yes', '2'=>'Dont know'], '', ['class' => 'form-control']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label style="float:right"><i>If yes What were the Results</i></label>
                                </td>
                                <td>
                                    {{ Form::select('partner_results', [''=>'--Select--','0'=>'HIV Negative', '1'=>'HIV positive', '2'=>'Unknown'], '', ['class' => 'form-control']) }}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-8">
                        <h3>Consent for testing (Parent/Guardian if child)</h3>
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td>
                                        <label>Do you agree to have his/her blood drawn for testing?</label>
                                    </td>
                                    <td>
                                        {{ Form::select('partner_results', [''=>'--Select--','1'=>'Yes', '0'=>'No'], '', ['class' => 'form-control']) }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-4"></div>

                    <div class="col-sm-12">
                        <h3>Section C: Test Results</h3>
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>
                                        <label>HIV final test results:</label>
                                    </td>
                                    <td>
                                        {{ Form::select('partner_results', [''=>'--Select--','1'=>'NEG', '2'=>'POS','3'=>'INC','4'=>'NT'], '', ['class' => 'form-control']) }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>Sent to EQA or Confirmatory Laboratory</label>
                                    </td>
                                    <td>
                                        {{ Form::select('confirmatory_lab', [''=>'--Select--','1'=>'Yes', '0'=>'No'], '', ['class' => 'form-control']) }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-12">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td>
                                    <label>Results Recieved:</label>
                                </td>
                                <td>
                                    {{ Form::select('results_received', [''=>'--Select--','1'=>'Yes', '0'=>'No'], '', ['class' => 'form-control']) }}
                                </td>
                            </tr>
                            <!-- if the patient is above 9 years -->
                            <tr>
                                <td>
                                    <label>Results received as a couple:</label>
                                </td>
                                <td>
                                    {{ Form::select('results_received_as_a_couple', [''=>'--Select--','1'=>'Yes', '0'=>'No'], '', ['class' => 'form-control']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Couple results:</label>
                                </td>
                                <td>
                                    {{ Form::select('results_received_as_a_couple', [''=>'--Select--','discordant'=>'Discordant', 'concordant'=>'Concordant'], '', ['class' => 'form-control']) }}
                                </td>
                            </tr>
                            <!-- ========end if================= -->
                            <tr>
                                <td>
                                    <label>Is there suspision for TB<br/>(Currentcough,fever,weightloss and
                                    nightsweats):</label>
                                </td>
                                <td>
                                    {{ Form::select('is_there_suspision_for_tb', [''=>'--Select--','0'=>'No', '1'=>'Yes'], '', ['class' => 'form-control']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Has client started cotrimoxazole prophylaxis:</label>
                                </td>
                                <td>
                                    {{ Form::select('has_client_started_cotrimoxazole_prophylaxis', [''=>'--Select--','0'=>'No', '1'=>'Yes'], '', ['class' => 'form-control']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Has client been linked to care or any other services?:</label>
                                </td>
                                <td>
                                    {{ Form::select('has_client_been_linked_to_care', [''=>'--Select--','0'=>'No', '1'=>'Yes'], '', ['class' => 'form-control']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Where?:</label>
                                </td>
                                <td>
                                    {{ Form::text('where_is_the_client_care','',['class' => 'form-control', 'data-error'=>'','placeholder'=>'']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>CD4 count results if applicable:</label>
                                </td>
                                <td>
                                    {{ Form::text('cd4_count_results','',['class' => 'form-control', 'data-error'=>'','placeholder'=>'']) }}
                                </td>
                            </tr>
                            <includeIf velocityTest="$fn.globalProperty('ugandaemr.hts.recency') == true">
                                <tr>
                                    <td>
                                        <label>HIV Recency Test Result:</label>
                                        <td>
                                            {{ Form::select('hiv_recency_test_result',[''=>'--Select--','1'=>'Recent','2'=>'Long Term'],'',['class' => 'form-control', 'data-error'=>'','placeholder'=>'']) }}
                                        </td>
                                    </td>
                                </tr>
                            </includeIf>
                            <tr>
                                <td>
                                    <label>
                                        <span class="required">*</span>Counsellor
                                    </label>
                                </td>
                                <td>
                                    {{ Form::text('counsellor','',['class' => 'form-control required', 'data-error'=>'','placeholder'=>'counsellor']) }}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-12">
                        <div style="float: right;">{{ Form::submit('Save',['class'=>'btn btn-success']) }}</div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection 

@push('scripts')
   <script src="{{ asset('elite/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
   <script type="text/javascript">
       jQuery('#counselling_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy'
        });
   </script>
@endpush