@extends('layouts.app')

@section('content')
	
	<div class="container">
		<div>
			<div class="row">
				<div class="full right">
					<h2>OUT PATIENT DEPARTMENT (OPD)</h2>	
				</div>
				<div class="full left">
					<a href="{{ route('OPD.create') }}">
						<button class="btn btn-primary"> ADD PATIENT</button>
					</a>
				</div>
			
			</div>
		</div>
		
		
		<table class="table table-bordered" id="table_id">
			<thead>
				<th width="5%">id</th>
				<th width="12%">Serial Number</th>
				<th>'M.A.U.C'</th>
				<th>M.U.C.A(cm)</th>
				<th>BMI</th>
				<th>Age of weight Zscore</th>
				<th>Height of age Zscore</th>
				<th class="text-center">
					
				</th>
			</thead>
			<?php  $no=1;?>
			<tbody>
				@foreach($patients as $patient)
					<tr>
						<td>{{ $no++ }}</td>
						<td>{{ $patient->serial_number}}</td>
						<td>{{ $patient->mauc }}</td>
						<td>{{ $patient->muac_cm }}</td>
						<td>{{ $patient->bmi }}</td>
						<td>{{ $patient->age_of_weight_zscore}}</td>
						<td>{{ $patient->height_of_age_zscore}}</td>
						<td>
							<a href="{{ route('OPD.show', $patient->id)}}">
							<button class="btn btn-primary">DETAILS</button>
							</a>
							<a href="{{ route('OPD.edit', $patient->id)}}">
								<button class="btn btn-success">EDIT</button>
							</a>
							<a href="{{ route('OPD.destroy', $patient->id)}}">
								<button class="btn btn-danger">DELETE</button>
							</a>
						</td>
					</tr>

				@endforeach
			</tbody>
				
			
		</table>

	</div>
@endsection
@push('scripts')
<script>
$(document).ready( function () {
    $('#table_id').DataTable();
</script>
@endpush