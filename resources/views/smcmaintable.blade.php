@extends('layouts.app')

@section('table2')
<div class="container">
 @include('message')
    <a href="/malecircumsion2/create" class="btn btn-info">Add Patient</a>
        <h2 class="pull-center">Circumcised Patient Follow up List</h2>
        <table class="table table-bordered table-striped" id="users-table">
    <thead>
    <tr class="headers" style="text-align: center;">

    <th colspan="3" scope="colgroup">Facility Information</th>
    <th colspan="2" scope="colgroup">Client Information</th>
    <th colspan="9" scope="colgroup">Knowledge Of HIV Status</th>
    <th colspan="11" scope="colgroup">Medical History</th>
    <th colspan="5" scope="colgroup">Client Udergoing Treatment</th>
    <th colspan="3" scope="colgroup">Allegies</th>
    <th colspan="18" scope="colgroup">Physical Exam</th>
    <th colspan="2" scope="colgroup">Action</th>
   
    
    </tr>
    
    <tr>
                <th scope="col">Site Type</th>
                <th scope="col">National ID</th>
                <th scope="col">Serial ID</th>
                <th scope="col">How You Heard About SMC</th>
                <th scope="col">Care Entry Point</th>
                <th scope="col">Sexually Active</th>
                <th scope="col">Tested For HIV Last Month</th>
                <th scope="col">HCT Offered</th>
                <th scope="col">Tested For HIV During Appointment</th> 
                <th scope="col">HIV Test Results</th>
                <th scope="col">HIV+ Incare</th>
                <th scope="col">Referred</th>
                <th scope="col">Tetanus Vaccination(Date:TT1)</th>
                <th scope="col">Tetanus Vaccination(Date:TT2)</th>
                <th scope="col">Bleeding Disorder</th>
                <th scope="col">Genital Ulcers</th>
                <th scope="col">Urethral Discharge</th>
                <th scope="col">Penile Warts</th>
                <th scope="col">Pain On Urination</th>
                <th scope="col">DifficultIn Foreskin</th>
                <th scope="col">Scrotum Swelling</th>
                <th scope="col">Electile Dysfunction</th>
                <th scope="col">Any Other</th>
                <th scope="col">Hypertension</th>
                <th scope="col">Anaemia</th>
                <th scope="col">Diabetes</th>
                <th scope="col">HIV/AIDS</th>
                <th scope="col">Any other</th>
                <th scope="col">Local Anesthetics</th>
                <th scope="col">Antiseptics</th>
                <th scope="col">Other Medication</th>
                <th scope="col">BP:Systolic</th>
                <th scope="col">BP:Diastolic</th>
                <th scope="col">Pulse(rate/min)</th>
                <th scope="col">Temp(^C)</th>
                <th scope="col">RR</th>
                <th scope="col">Weight(Kgs)</th>
                <th scope="col">Urethra Discharge</th>
                <th scope="col">Adhensions</th>
                <th scope="col">Anatomical Abnormalities</th>
                <th scope="col">GUD</th>
                <th scope="col">Balanitis</th>
                <th scope="col">Genital Warts</th>
                <th scope="col">Surgical Disorders</th>
                <th scope="col">OtherSTI</th>
                <th scope="col">OpenWounds/Healed Scars</th>
                <th scope="col">Jiggers</th>
                <th scope="col">Any Other</th>
                <th scope="col">Treatment Given</th>
                <th scope="col">In Good Health</th>
                <th scope="col">Counseled About SMC</th>
                <th scope="col">More</th>
                <th scope="col">Edit/Delete</th>
              
    </tr>
</thead>
<tbody>

</table>
</div>
@endsection
@push('scripts')
<script>
$(document).ready(function(){
    $('#users-table').DataTable({
        processing:true,
        serverSide:true,
        ajax:{
            url:"{{route('malecircumsion2.index')}}",
        },
        columns:[
            {
                data:'sitetype',
                name:'sitetype',

            },
            {
                data:'NatID',
                name:'NatID',
            },
            {
                data:'SerID',
                name:'SerID',
            },
            {
                data:'hw_u_heard_smc',
                name:'hw_u_heard_smc',
            },
            {
                data:'careentrypoint',
                name:'careentrypoint',
            },
            {
                data:'sexuallyactive',
                name:'sexuallyactive',
            },
            {
                data:'testbefore',
                name:'testbefore',
               
            },
            {
                data:'HTCoffered',
                name:'HTCoffered',
                
            },
            {
                data:'testnow',
                name:'testnow',
                
            },
            {
                data:'testresults',
                name:'testresults',
                
            },
            {
                data:'incare',
                name:'incare',
            
            },
            {
                data:'reffered',
                name:'reffered',
                
            },
            {
                data:'TT1',
                name:'TT1',
                
            },
            {
                data:'TT2',
                name:'TT2',
                
            },
            {
                data:'bleeding',
                name:'bleeding',
                
            },
            {
                data:'ulcers',
                name:'ulcers',
                
            },
            {
                data:'urethral',
                name:'urethral',
                
            },
            {
                data:'penile_wts',
                name:'penile_wts',
                
            },
            {
                data:'pain_on_urn',
                name:'pain_on_urn',
            
            },
            {
                data:'diff_in_foreskin',
                name:'diff_in_foreskin',
                
            },
            {
                data:'swelling_scrotum',
                name:'swelling_scrotum',
                
            },
            {
                data:'elec_dysfunction',
                name:'elec_dysfunction',
                
            },
            {
                data:'other_specify',
                name:'other_specify',
                orderable:false
            },
            {
                data:'hypertension',
                name:'hypertension',
                
            },
            {
                data:'anaemia',
                name:'anaemia',
            
            },
            {
                data:'diabetes',
                name:'diabetes',
                
            },
            {
                data:'HIV_AIDS',
                name:'HIV_AIDS',
                
            },
            {
                data:'other_spec',
                name:'other_spec',
                
            },
            {
                data:'local_anesthetics',
                name:'local_anesthetics',
            
            },
            {
                data:'Antiseptics',
                name:'Antiseptics',
                
            },
            {
                data:'other_medic',
                name:'other_medic',
                
            },
            {
                data:'Systolic',
                name:'Systolic',
                
            },
            {
                data:'Diastolic',
                name:'Diastolic',
                
            },
            {
                data:'pulse',
                name:'pulse',
                
            },
            {
                data:'temp',
                name:'temp',
                
            },
            {
                data:'RR',
                name:'RR',
                
            },
            {
                data:'weight',
                name:'weight',
            
            },
            {
                data:'urethra_d',
                name:'urethra_d',
                
            },
            {
                data:'Adhesion',
                name:'Adhesion',
                
            },
            {
                data:'Anotomical_abns',
                name:'Anotomical_abns',
                
            },
            {
                data:'GUD',
                name:'GUD',
                
            },
            {
                data:'Blanitis',
                name:'Blanitis',
            
            },
            {
                data:'Genital_wts',
                name:'Genital_wts',
                
            },
            {
                data:'Surg_disorders',
                name:'Surg_disorders',
                
            },
            {
                data:'STI_abns',
                name:'STI_abns',
                
            },
            {
                data:'openwound',
                name:'openwound',
            
            },
            {
                data:'jiggers',
                name:'jiggers',
                
            },
            {
                data:'otherspecif',
                name:'otherspecif',
                
            },
            {
                data:'TTgiven',
                name:'TTgiven',
                
            },
            {
                data:'goodhealth',
                name:'goodhealth',
                
            },
            {
                data:'cc_smc',
                name:'cc_smc',
                
            },
            {
                data:'more',
                name:'more',
               
            },
            {
                data:'action',
                name:'action',
                orderable:false
            }
        ]
    });
    $('#users-table').on('click', '.btn-delete[data-remote]', function (e) { 
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var url = $(this).data('remote');
    // confirm then
    if (confirm('Are you sure you want to delete this?')) {
    $.ajax({
        url: url,
        type: 'DELETE',
        dataType: 'json',
        data: {method: '_DELETE', submit: true}
    }).always(function (data) {
        $('#users-table').DataTable().draw(false);
    }); }else
        alert("You have cancelled!");
});
});
</script>
@endpush